﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    Rigidbody rigid2D;
    float jumpForce = 780.0f;
    float walkForce = 30.0f;
    float maxWalkSpeed = 2.0f;

    // Start is called before the first frame update
    void Start()
    {
        this.rigid2D = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) && this.rigid2D.velocity.y == 0)
        {

            Rigidbody rb = this.GetComponent<Rigidbody>();  // rigidbodyを取得
            Vector3 Upforce = new Vector3(0.0f,jumpForce, 0.0f);    // 力を設定
            rb.AddForce(Upforce);  // 力を加える
        }

        int Xkey = 0;
        int Zkey = 0;
        if (Input.GetKey(KeyCode.RightArrow)) Xkey = 1;
        if (Input.GetKey(KeyCode.LeftArrow)) Xkey = -1;
        if (Input.GetKey(KeyCode.UpArrow)) Zkey = 1;
        if (Input.GetKey(KeyCode.DownArrow)) Zkey = -1;

        float speedx = Mathf.Abs(this.rigid2D.velocity.x);
        float speedz = Mathf.Abs(this.rigid2D.velocity.z);

        if (speedx < this.maxWalkSpeed)
        {
            Rigidbody rb = this.GetComponent<Rigidbody>();  // rigidbodyを取得
            Vector3 Sideforce = new Vector3((Xkey * walkForce),0.0f, 0.0f);    // 力を設定
            rb.AddForce(Sideforce);  // 力を加える
        }
        if (speedz < this.maxWalkSpeed)
        {
            Rigidbody rb = this.GetComponent<Rigidbody>();  // rigidbodyを取得
            Vector3 Flontforce = new Vector3(0.0f, 0.0f,(Zkey * walkForce));    // 力を設定
            rb.AddForce(Flontforce);  // 力を加える
        }

        if (Xkey != 0)
        {
            transform.localScale = new Vector3(Xkey, 1, 1);
        }
        if (Zkey != 0)
        {
            transform.localScale = new Vector3(1, 1, Zkey);
        }

       
        if (transform.position.y < -10)
        {
            SceneManager.LoadScene("GameScene");
        }
    }

    void OnTriggerEnter(Collider other)
    {
        Debug.Log("ゴール");
        SceneManager.LoadScene("ClearScene");
    }
}
